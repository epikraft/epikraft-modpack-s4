# EpiKraft Modpack - Säsong 4
I detta repo finner du källkoden till EpiKrafts modpack för säsong 4.
Du kan hitta changelog och nedladdningar av gamla versioner av modpacket
under [releases](https://gitlab.com/epikraft/epikraft-modpack-s4/-/releases). Instruktioner för hur man installerar modpacket finnns [tillgängliga här](INSTALLING.md).

## Mod-lista
*Se [`modpack/config/`](modpack/config/) och [`modpack/defaultconfigs/`](modpack/defaultconfigs/) för avstängda features i modsen.*

- [Create](https://www.curseforge.com/minecraft/mc-mods/create)
- [Immersive Engineering](https://www.curseforge.com/minecraft/mc-mods/immersive-engineering)
- [CC: Tweaked](https://www.curseforge.com/minecraft/mc-mods/cc-tweaked)
- [Abnormals Core](https://www.curseforge.com/minecraft/mc-mods/abnormals-core)
  - [Atmospheric](https://www.curseforge.com/minecraft/mc-mods/atmospheric)
  - [Environmental](https://github.com/team-abnormals/announcements/blob/main/environmental_announcement.md)
    - Byggt från [källkoden](https://github.com/team-abnormals/environmental) eftersom det inte har släppts än
  - [Autumnity](https://www.curseforge.com/minecraft/mc-mods/autumnity)
  - [The Endergetic Expansion](https://www.curseforge.com/minecraft/mc-mods/endergetic)
- [AutoRegLib](https://www.curseforge.com/minecraft/mc-mods/autoreglib)
  - [Quark](https://www.curseforge.com/minecraft/mc-mods/quark)
- [Supplementaries](https://www.curseforge.com/minecraft/mc-mods/supplementaries/files)
- [Double Slabs](https://www.curseforge.com/minecraft/mc-mods/double-slabs)
- [mGui](https://www.curseforge.com/minecraft/mc-mods/mgui)
  - [Tetra](https://www.curseforge.com/minecraft/mc-mods/tetra)
- [Dash](https://www.curseforge.com/minecraft/mc-mods/dash)
- [Leap](https://www.curseforge.com/minecraft/mc-mods/leap)
- [Repurposed Structures](https://www.curseforge.com/minecraft/mc-mods/repurposed-structures)
- [Serene Seasons](https://www.curseforge.com/minecraft/mc-mods/serene-seasons)
- [Biomes O' Plenty](https://www.curseforge.com/minecraft/mc-mods/biomes-o-plenty)
- [YUNG's API](https://www.curseforge.com/minecraft/mc-mods/yungs-api)
  - [YUNG's Better Caves](https://www.curseforge.com/minecraft/mc-mods/yungs-better-caves)
  - [YUNG's Better Mineshafts](https://www.curseforge.com/minecraft/mc-mods/yungs-better-mineshafts-forge)
- [Just Enough Items (JEI)](https://www.curseforge.com/minecraft/mc-mods/jei)
- [The One Probe](https://www.curseforge.com/minecraft/mc-mods/the-one-probe)
- [Server Tab Info](https://www.curseforge.com/minecraft/mc-mods/server-tab-info)
- [AI Improvements](https://www.curseforge.com/minecraft/mc-mods/ai-improvements)
- [Placebo](https://www.curseforge.com/minecraft/mc-mods/placebo)
  - [FastWorkbench](https://www.curseforge.com/minecraft/mc-mods/fastworkbench)
  - [FastFurnace](https://www.curseforge.com/minecraft/mc-mods/fastfurnace)
- [OptiFine](https://www.optifine.net/home)

## Filer
Modpackets källkod ligger under mappen [`modpack/`](modpack/), med följande struktur:
- [`bin/modpack.jar`](modpack/bin/modpack.jar) - Modpackets Forge-installation
- [`bin/runData`](modpack/bin/runData) - Modpackets krav (läses av Technic Launcher)
- [`config/`](modpack/config/) och [`defaultconfigs/`](modpack/defaultconfigs/) - Konfiguration av mods
- [`mods/`](modpack/mods/) - Alla mods i modpacket
- [`servers.dat`](modpack/servers.dat) - Standardinställda servrar
  - Så att EpiKraft automatiskt hamnar i server-listan
