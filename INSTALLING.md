# Hur man installerar modpacket
1. Kontrollera att du har Java installerat (version 8 eller högre).
Det enklaste sättet är att öppna en terminal/konsol och köra `java -version`.
Om du inte har det, installera Java 8 eller högre från valfri leverantör t.ex. [AdoptOpenJDK](https://adoptopenjdk.net).

Om du vill ha full kompatibilitet med äldre modpacks (t.ex. EpiKraft säsong 2) behöver du ladda ned Java 8.
Java 9+ fungerar endast på nyare versioner av Forge, för Minecraft 1.14 och uppåt (alltså kommer det fungera för detta modpack).
Dock kommer Technic Launcher ge dig en varning när du startar modpacket, men det är bara att ignorera.
Rent generellt har nyare versioner av Java bättre performance, så om du endast är intresserad av att köra
detta modpack skulle jag rekommendera att du installlerar JDK11 från [AdoptOpenJDK](https://adoptopenjdk.net),
eller från din pakethanterare om du använder Linux.

2. Installera Technic Launcher om du inte redan har gjort det.
Nedladdningar för Windows, macOS och Linux finns på
[https://www.technicpack.net/download](https://www.technicpack.net/download), längst ned på sidan.

**Notera** att om du är på Windows kommer `.exe`-varianten av launchern *endast* att fungera med Oracles JRE 8.
Om du t.ex. har installerat AdoptOpenJDK istället skulle jag rekommendera att ladda ned `.jar`-varianten av launchern,
vilket du får om du laddar ned från macOS/Linux-knappen (den funkar på Windows också).

Det finns ingen bra anledning att använda Oracles JRE 8 i dagens läge, den är gammal och övergiven,
och dessutom är det ju trevligare att använda programvara med öppen källkod.
Dessutom kan modernare versioner av Java vara mycket snabbare än 8an, tack vare optimeringar av GC-motorn.

3. Starta Technic Launcher och logga in med ditt Minecraft-konto.

4. Klistra in länken `http://api.technicpack.net/modpack/epikraft-modpack-s4` i sökrutan under fliken "Modpacks".

5. Klicka på "Install" på modpack-sidan, längst ned i högra hörnet.

6. Klicka på "Play" för att spela modpacket.

Om du får upp en dialogruta som berättar att du behöver allokera mer RAM-minne till Java bör du acceptera genom att trycka på "OK", annars kan det hända att spelet får minnesbrist och kraschar eller laggar till följd av det. Du kan även kryssa i "Don't Ask Me Again" för att launchern i fortsättningen automatiskt ska konfigurera minnet utan att fråga.

När modpacket uppdateras kommer det synas i Technic Launcher, och du kommer då bli tillfrågad om du vill uppdatera när du trycker på "Play". Servern är alltid på senaste versionen, så du bör uppdatera om det finns en uppdatering tillgänglig.

## Manuell installation av modpacket
Om du av någon anledning inte vill använda Technic Launcher kan du även installera modpacket manuellt.
Om du redan vet hur man installerar Forge och mods manuellt bör instruktionerna inte vara några konstigheter, annars rekommenderar jag att använda Technic Launcher.
Du kommer behöva mixtra med alla filer självt och dessutom göra om det varje gång jag släpper en uppdatering.

1. Ladda ned modpackets ZIP-paket från någon version under [releases](https://gitlab.com/epikraft/epikraft-modpack-s4/-/releases).

2. Extrahera ZIP-filen på valfri plats.

3. Kör Forge-installationen för en klient (`bin/modpack.jar`).

4. Kopiera allting som fanns i ZIP-paketet förutom `bin/` till din `.minecraft`-mapp.

5. Öppna Minecraft-launchern och spela modpacket genom att välja Forge-installationen.
